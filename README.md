Instructions
=============

	-> You can add a color to list
	
		1. type the HEX number of the color
		2. click on Add button
	
		Cases:
			
			a. You did not type a HEX number => this is not a color
			b. The HEX number you typed already exists in the list => color already exists
			c. You typed a color => color added to list
			
	-> You can edit the colors of the list
	
		1. click on the color you want to edit
		2. change the HEX number
		3. click on Save button
		
		Cases:
			
			a. You did not type a HEX number => this is not a color
			b. The HEX number you typed already exists in the list => color already exists
			c. You typed a color => color added to list
			
	-> You can delete a color from the list
	
		1. click on Close button (X) of the color you want to delete
		
	* Special cases:
	
		1. If you click on a color in order to edit it and the click on the Add button the function for addition
			will be called and a "Color #XXXXXX already exists in the list" message will appear.
			
		2. If you click on a color in order to edit it and the click on the Save button the function for saving change
			will be called, but there is no change, so a "New color is not selected" message will appear.
			
		3. If you click on a color in order to edit it, change the HEX number and click on the Add button the function for 
			addition will be called.