import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Color extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      color: '',
      color_to_replace: '',
      new_color: '',
      colors: Array(),
      list: Array(),
      action: 'add',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
        
  }

  handleChange(event) {
    this.setState({color: event.target.value});
    this.setState({new_color: event.target.value});
  }
  
  resetColor(){
    this.setState({color: ''});
  }
  
  resetColors(){
    this.setState({color: ''});
    this.setState({color_to_replace: ''});
    this.setState({new_color: ''});
  }
  
  selectColor(color){
    this.setState({action: 'replace'});
    this.setState({color: color});
    this.setState({color_to_replace: color});
  }
  
  replaceColor(){
    if(this.state.color_to_replace!=''){
      if(this.state.new_color!=''){
		var isColor  = /^#[0-9A-F]{6}$/i.test('#' + this.state.new_color);
		if(isColor){
		  var cell = this.state.colors.indexOf(this.state.new_color);
		  if(cell==-1){
		    var cell_ = this.state.colors.indexOf(this.state.color_to_replace);
		    this.state.colors.splice(cell_ , 1 , this.state.new_color);
		    this.setState({list: Array()});
		    this.createList();
		} else {
		  alert('Color #' + this.state.new_color + ' already exists in the list');
		}
		} else {
		  alert('#' + this.state.color + ' is not a color');
		}
      } else {
        alert('New color is not selected');
      }
    } else {
      alert('Color is not selected');
    }
  }
  
  deleteColor(color) {
    this.setState({color: ''});
    var hex = color.color;
    var cell = this.state.colors.indexOf(hex);
    if(cell!=-1){
      this.state.colors.splice(cell,1);
      this.setState({list: Array()});
      this.createList();
    }
  }

  handleSubmit(event) {
    
    var color = this.state.color;
    
    if(color==''){
        alert('Please type a color\'s HEX');
    } else {
      
        var isColor  = /^#[0-9A-F]{6}$/i.test('#' + color);
      
        if(isColor){
          
          var cell = this.state.colors.indexOf(color);
          
          if(cell==-1){
           this.state.colors.push(this.state.color);
           this.createList();
          } else {
            alert('Color #' + color + ' already exists in the list');
          }
          
        } else {
          alert('#' + this.state.color + ' is not a color');
        }
        
    }
   
    this.setState({color: ''});
    this.setState({color_to_replace: ''});
    this.setState({new_color: ''});
    event.preventDefault();
    
  }
  
  createList(){
   const list = this.state.colors.map((color)=>{ 
      return <li className='list' key={color.toString()}>
        <div className='left-td'>
          <div className='color-div' style={{backgroundColor: "#" + color}}  onClick={()=> this.selectColor(color)}></div><button className="delete-btn" onClick={() => this.deleteColor({color})}>X</button>
        </div>
      </li>;
    }); 
    this.setState({list: list});
    this.resetColors();
  }
  
  saveColor(color){
    alert(JSON.stringify(color));
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            #<input className="color-input" type="text" maxLength="6" value={this.state.color} onClick={() => this.resetColor()} onChange={this.handleChange} />
          </label>
          <input className="add-btn" type="submit" value="Add" />
          <button type="button" className="save-btn" onClick={() => this.replaceColor()}>Save</button>
        </form>
        <ul>
          {this.state.list}
        </ul>
      </div>
    );
  }
}

ReactDOM.render(
  <Color />,
  document.getElementById('root')
);